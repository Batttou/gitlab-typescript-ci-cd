const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'gitlab-typescript-ci-cd'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/gitlab-typescript-ci-cd-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'gitlab-typescript-ci-cd'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/gitlab-typescript-ci-cd-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'gitlab-typescript-ci-cd'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/gitlab-typescript-ci-cd-production'
  }
};

module.exports = config[env];
